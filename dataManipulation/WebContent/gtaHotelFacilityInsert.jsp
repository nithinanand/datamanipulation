<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <head>
  <jsp:include page="header.jsp"></jsp:include>
  
  <jsp:include page="nav.jsp"></jsp:include> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    
    
     <div class="container">
       <br><br><br>
       <div class="row">
           <div class="panel panel-default ">
  <div class="panel-heading">
    <h3 class="panel-title center">Add Row</h3>
  </div>
  </div>
  </div>
        <form action="gtaHotelFacilityInsertdb.jsp" method="post">
            <table>
              
                <tbody>
                    <tr>
                        <td class="lwidth"><label>Facility code</label></td>
                       
                        <td><input type="text" name="fcode"/></td>
                    </tr>
                     
                    <tr>
                   
                        <td class="lwidth"><label>Description</label></td>
                        <td><input type="text" name="desc"/></td>
                    </tr>
                    
                       
                </tbody>
            </table>
            <br>
             <input type="submit" value="Save" class="zcolor btn-width"/>
                        <input type="reset" value="Reset" class="zcolor btn-width"/>
                    
        </form>
        <br>
        <a href="gtaHotelFacility.jsp"><input type="button" value="Back" class="zcolor btn-width"/></a>
        </div>
        <font color="red"><c:if test="${not empty param.errMsg}">
            <c:out value="${param.errMsg}" />
           
        </c:if></font>
        <font color="green"><c:if test="${not empty param.susMsg}">
            <c:out value="${param.susMsg}" />
            
        </c:if></font>
 
    </body>
</html>