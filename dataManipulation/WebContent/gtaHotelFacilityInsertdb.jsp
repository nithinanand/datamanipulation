<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
 
<html>
    <head>
        <head>
  <jsp:include page="header.jsp"></jsp:include>
        <title>INSERT Operation</title>
    </head>
    <body>
        <c:if test="${ empty param.fcode or empty param.desc}">
            <c:redirect url="gtaHotelFacilityInsert.jsp" >
                <c:param name="errMsg" value="Please Enter Facility code and Description" />
            </c:redirect>
 
        </c:if>
        <sql:setDataSource var="dbsource" driver="com.mysql.jdbc.Driver"
                           url="jdbc:mysql://localhost/test"
                           user="root"  password="toor"/>
 
 
        <sql:update dataSource="${dbsource}" var="result">
            INSERT INTO gta_hotel_facility(facility_code, EN) VALUES (?,?);
            <sql:param value="${param.fcode}" />
            <sql:param value="${param.desc}" />
        </sql:update>
        <c:if test="${result>=1}">
            <font size="5" color='green'>Data inserted
            successfully.</font>
 
            <c:redirect url="gtaHotelFacilityInsert.jsp" >
                <c:param name="susMsg" value="Data inserted
            successfully." />
            </c:redirect>
        </c:if> 
 
 
    </body>
</html>