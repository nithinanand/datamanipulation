<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html>
<html>
    <head>
        <head>
          
  <jsp:include page="header.jsp"></jsp:include>
  <jsp:include page="nav.jsp"></jsp:include> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
 
        <sql:setDataSource var="dbsource" driver="com.mysql.jdbc.Driver"
                           url="jdbc:mysql://localhost/test"
                           user="root"  password="toor"/>
 
        <sql:query dataSource="${dbsource}" var="result">
            SELECT * from gta_hotel_facility where id=?;
            <sql:param value="${param.id}" />
        </sql:query>
        <br><br><br>
        
         <div class="container">
       <br><br><br>
       <div class="row">
           <div class="panel panel-default ">
  <div class="panel-heading">
    <h3 class="panel-title center">Modify</h3>
  </div>
  </div>
  </div>
        
        <form action="gtaHotelFacilityUpdatedb.jsp" method="post">
            <table border="0" width="40%">
                
                <tr>
                    <th>Facility code</th>
                    <th>Description</th>
                </tr>
                <c:forEach var="row" items="${result.rows}">
                    <tr>
                        <td><input type="hidden" value="${param.id}" name="id"/>
                            <input type="text" value="${row.facility_code}" name="fcode"/></td>
                        <td><input type="text" value="${row.EN}" name="desc"/></td>
                        <td></td>
                    </tr>
                </c:forEach>
            </table>
            <br><br>
            <a href="gtaHotelFacility.jsp"><input type="button" value="Back" class="zcolor btn-width"/></a>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="submit" value="Update" class="zcolor btn-width"/>
        </form>
        </div>
    </body>
</html>