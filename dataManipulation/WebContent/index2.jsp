<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
  <jsp:include page="header.jsp"></jsp:include> 
  <jsp:include page="nav.jsp"></jsp:include> 
  </head>
    <body>
    <div class="container">
    <br><br><br>
        <h2 class="color">Select a Table to view/modify</h2>
        
        <select id="dynamic_select" class="input-group">
  <option value="" selected>Table Name</option>
  <option value="display.jsp">z_pricing</option>
  <option value="gtaHotelFacility.jsp?userId=${'gtahotelfacility'}">gta hotel facility</option>
  <option value="#">Test</option>
</select>

        </div>
        
 
        <script>
    $(function(){
      // bind change event to select
      $('#dynamic_select').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
    </body>
</html>