<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script   src="https://code.jquery.com/jquery-3.0.0.min.js"   integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0="   crossorigin="anonymous"></script>
    </head>
    <body>
        <h1>Choose Option</h1>
        <a href="insert.jsp">Insert Record</a><p></p>
        <select id="dynamic_select">
  <option value="" selected>Pick a DB</option>
  <option value="display.jsp">z_pricing</option>
  <option value="gtaHotelFacility.jsp?userId=${'gtahotelfacility'}">gta hotel facility</option>
  <option value="https://www.gurustop.net">GuruStop.NET</option>
</select>
        <a href="display.jsp">Display Record</a>
        
        <script>
    $(function(){
      // bind change event to select
      $('#dynamic_select').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
    </body>
</html>